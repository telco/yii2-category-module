category manager
================
Category Module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist saghar/yii2-category-module "*"
```

or add

```
"saghar/yii2-category-module": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \saghar\category\AutoloadExample::widget(); ?>```