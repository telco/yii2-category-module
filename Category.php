<?php

namespace saghar\category;

/**
 * categoryManager module definition class
 */
class Category extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'saghar\category\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
